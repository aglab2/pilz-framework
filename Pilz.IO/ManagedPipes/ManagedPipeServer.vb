﻿Imports System.Net
Imports System.IO.Pipes
Imports Pilz.Threading

Public Class ManagedPipeServer : Inherits ManagedPipe

    'Pro Verbindung (Anfrage) wird ein Client-Objekt generiert, das den Datenaustausch dieser Verbindung abwickelt
    Public ReadOnly Property Clients As New List(Of ManagedPipeClient)
    Private ReadOnly pipeName As String = ""
    Private ReadOnly maxNumbersOfServerInstances As Integer
    Private numberOfStartedServerInstances As Integer = 0

    Public Sub New(ByVal pipeName As String)
        Me.New(pipeName, 1)
    End Sub

    Public Sub New(pipeName As String, maxNumbersOfServerInstances As Integer)
        Me.pipeName = pipeName
        Me.maxNumbersOfServerInstances = maxNumbersOfServerInstances
        CreateWaitingStream()
    End Sub

    Private Sub CreateWaitingStream()
        If numberOfStartedServerInstances < maxNumbersOfServerInstances Then
            Dim strm = New NamedPipeServerStream(pipeName, PipeDirection.InOut, maxNumbersOfServerInstances, PipeTransmissionMode.Byte, PipeOptions.Asynchronous)
            numberOfStartedServerInstances += 1
            strm.BeginWaitForConnection(AddressOf EndAccept, strm)
            Console.WriteLine("Start Waiting for new Connection ...")
        End If
    End Sub

    Private Sub EndAccept(ByVal ar As IAsyncResult)
        Dim strm = DirectCast(ar.AsyncState, NamedPipeServerStream)

        strm.EndWaitForConnection(ar)

        If IsDisposed Then
            strm.Dispose()
            Return
        End If

        With New ManagedPipeClient(strm)
            AddHandler .RetriveData, AddressOf Client_RetriveData
            AddHandler .StatusMessage, AddressOf Client_StatusMessage
            AddHandler .Disposed, AddressOf Client_Disposed
            .AddTo(_Clients)
        End With

        Console.WriteLine("Client accepted!")

        CreateWaitingStream()
    End Sub

#Region "_Clients-Ereignisverarbeitung"

    Private Sub Client_Disposed(ByVal Sender As ManagedPipe)
        'den Client für die beendete Verbindung entfernen
        Sender.RemoveFrom(_Clients)
        numberOfStartedServerInstances -= 1
        CreateWaitingStream()
    End Sub

    Private Sub Client_RetriveData(ByVal sender As Object, ByVal e As DataEventArgs)
        'einkommende ChatMessages anzeigen, und an alle versenden
        OnRetriveData(e)
    End Sub

    Private Sub Client_StatusMessage(ByVal sender As Object, ByVal e As DataEventArgs)
        'einkommende StatusMessages durchreichen (zur Anzeige)
        OnStatusMessage(e)
    End Sub

#End Region

    Public Overrides Function SendAsnyc(bytes() As Byte) As Task
        Return Task.Run(Sub() Send(bytes))
    End Function

    Public Overrides Sub Send(data As Byte())
        For Each client As ManagedPipeClient In _Clients 'an alle versenden
            client.Send(data)
        Next
    End Sub

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If numberOfStartedServerInstances < maxNumbersOfServerInstances Then
            Using clnt As New NamedPipeClientStream(pipeName)
                'Herstellen einer Dummi-Verbindung, damit der ServerStream aus dem Wartezustand herauskommt.
                clnt.Connect()
            End Using
        End If

        For i As Integer = _Clients.Count - 1 To 0 Step -1
            _Clients(i).Dispose()
        Next
    End Sub

End Class
