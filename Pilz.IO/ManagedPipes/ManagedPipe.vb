﻿''' <summary>
''' stellt den Erben "Server" und "Client" 2 verschiedene
''' Message-Events zur Verfügung, und ein Event-Raisendes Dispose
''' </summary>
Public MustInherit Class ManagedPipe : Implements IDisposable

    Public Delegate Sub EventHandlerWithOneArgument(Of T0)(Sender As T0)

    ''' <summary>
    ''' Zur Ausgabe chat-verwaltungstechnischer Status-Informationen
    ''' </summary>
    Public Event StatusMessage As EventHandler(Of DataEventArgs)
    ''' <summary>Zur Ausgabe von Chat-Messages</summary>
    Public Event RetriveData As EventHandler(Of DataEventArgs)
    Public Event Disposed As EventHandlerWithOneArgument(Of ManagedPipe)

    Private _IsDisposed As Boolean = False

    Protected MustOverride Sub Dispose(disposing As Boolean)
    Public MustOverride Sub Send(bytes As Byte())
    Public MustOverride Function SendAsnyc(bytes As Byte()) As Task

    Protected Sub OnStatusMessage(ByVal e As DataEventArgs)
        RaiseEvent StatusMessage(Me, e)
    End Sub

    Protected Sub OnRetriveData(ByVal e As DataEventArgs)
        RaiseEvent RetriveData(Me, e)
    End Sub

    Public Sub RemoveFrom(Of T As ManagedPipe)(ByVal Coll As ICollection(Of T))
        Coll.Remove(DirectCast(Me, T))
    End Sub

    Public ReadOnly Property IsDisposed() As Boolean
        Get
            Return _IsDisposed
        End Get
    End Property

    Public Sub AddTo(Of T As ManagedPipe)(ByVal Coll As ICollection(Of T))
        Coll.Add(Me)
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        If _IsDisposed Then Return
        _IsDisposed = True
        Dispose(True)           ' rufe die erzwungenen Überschreibungen von Sub Dispose(Boolean)
        RaiseEvent Disposed(Me)
        GC.SuppressFinalize(Me)
    End Sub

End Class
