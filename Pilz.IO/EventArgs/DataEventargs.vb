﻿Public Class DataEventArgs : Inherits EventArgs

    Public ReadOnly Data As Byte()

    Public Sub New(bytes As Byte())
        MyBase.New()
        Data = bytes
    End Sub

End Class
