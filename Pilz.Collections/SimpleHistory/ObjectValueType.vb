﻿Namespace SimpleHistory

    ''' <summary>
    ''' Specify which member types you would include.
    ''' </summary>
    Public Enum ObjectValueType
        None = 0
        Field = 1
        [Property] = 2
    End Enum

End Namespace