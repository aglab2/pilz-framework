﻿Imports System.IO
Imports Newtonsoft.Json.Linq
Imports Pilz.Updating.Model
Imports Pilz.Updating.Scripts

Public Class UpdateInstaller

    Private restartHostApplication As Boolean
    Private autoCloseHostApplication As Boolean
    Private packageDir As String
    Private appExePath As String

    Private Sub UpdateInstaller_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        CheckCommandLines()

        If autoCloseHostApplication Then
            CloseHostApplication()
        End If

        InstallUpdates()

        If restartHostApplication Then
            StartHostApplication()
        End If
    End Sub

    Private Sub CloseHostApplication()
        Dim myProcesses As New List(Of Process)

        For Each p As Process In Process.GetProcesses
            If p.MainModule.FileName = appExePath Then
                myProcesses.Add(p)
            End If
        Next

        For Each p As Process In myProcesses
            p.Close()
        Next

        Dim waited As Byte = 5
        Do While myProcesses.Where(Function(n) Not n.HasExited).Any AndAlso waited > 0
            Sleep(1000)
            waited -= 1
        Loop

        For Each p As Process In myProcesses
            If Not p.HasExited Then
                p.Kill()
            End If
        Next
    End Sub

    Private Sub StartHostApplication()
        Process.Start(appExePath)
    End Sub

    Private Sub CheckCommandLines()
        For Each cmd As String In My.Application.CommandLineArgs
            Dim splitted As String() = cmd.Split("#")

            If splitted.Length = 2 Then
                Select Case splitted(0).ToLower
                    Case "restartHostApp".ToLower
                        restartHostApplication = splitted(1)
                    Case "autoCloseHostApp".ToLower
                        autoCloseHostApplication = splitted(1)
                    Case "updatePackage".ToLower
                        packageDir = splitted(1)
                    Case "applicationPath"
                        appExePath = splitted(1)
                End Select
            End If
        Next
    End Sub

    Private Sub InstallUpdates()
        Dim packagePath As String = Path.Combine(packageDir, "package.json")
        Dim packageFilesDir As String = Path.Combine(packageDir, "files")
        Dim systemFilesDir As String = Path.GetDirectoryName(appExePath)

        'Load update package
        Dim package As UpdatePackage = JObject.Parse(File.ReadAllText(packagePath)).ToObject(Of UpdatePackage)

        'Execute ScriptBefore
        ExecuteScriptsWithPriority(ScriptPriority.Before, package.Scripts, systemFilesDir, packageFilesDir)

        'Update Files
        For Each rFile As String In package.Fileupdates
            Dim packageFile As String = Path.Combine(packageFilesDir, rFile)
            Dim systemFile As String = Path.Combine(systemFilesDir, rFile)
            Dim systemFileDir As String = Path.GetDirectoryName(systemFile)

            'Create destination directory if not exists
            If Not Directory.Exists(systemFileDir) Then
                Directory.CreateDirectory(systemFileDir)
            End If

            'Copy package file to system file
            File.Copy(packageFile, systemFile, True)
        Next

        'ExecuteScriptAfter
        ExecuteScriptsWithPriority(ScriptPriority.After, package.Scripts, systemFilesDir, packageFilesDir)
    End Sub

    Private Sub ExecuteScriptsWithPriority(priority As ScriptPriority, scripts As IEnumerable(Of Script), applicationPath As String, updatePackagePath As String)
        For Each script As Script In scripts
            If script.Priority = priority Then
                Dim mgr As New ScriptManager(applicationPath, updatePackagePath)
                mgr.ExecuteScript(script)
            End If
        Next
    End Sub

End Class
