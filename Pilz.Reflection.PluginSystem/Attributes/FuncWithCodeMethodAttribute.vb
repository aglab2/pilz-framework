﻿Namespace Attributes

    Public Class PluginFunctionAttribute
        Inherits Attribute

        Public ReadOnly Property FunctionCode As String
        Public ReadOnly Property Params As Object()

        ''' <summary/>
        ''' <param name="funcCode">The function code for this PluginFunction.</param>
        ''' <param name="params">The parameters for this PluginFunction.</param>
        Public Sub New(funcCode As String, ParamArray params As Object())
            Me.FunctionCode = funcCode
            Me.Params = params
        End Sub

    End Class

End Namespace
