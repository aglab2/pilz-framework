﻿Imports System.Reflection

Public Class PluginFunction

    ''' <summary>
    ''' Gets the method to invoke when invoking this PluginFunction.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Method As MethodInfo

    ''' <summary>
    ''' Gets the refered Plugin for this PluginFunction, if it has one.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Plugin As Plugin

    ''' <summary>
    ''' Gets the Parameters that was given by the attribute.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Params As Object()

    ''' <summary>
    ''' Gets the function code for this PluginFunction.
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property FunctionCode As String

    ''' <summary>
    ''' Creates a new instance of a PluginFunction.
    ''' </summary>
    ''' <param name="method">The Method to invoke when invoking this PluginFunction.</param>
    ''' <param name="plugin">The Plugin that is the Parent of this PluginFunction. This value can be NULL.</param>
    Public Sub New(method As MethodInfo, plugin As Plugin)
        Me.Method = method
        Me.Plugin = plugin
    End Sub

    ''' <summary>
    ''' Creates a new instance of a PluginFunction.
    ''' </summary>
    ''' <param name="method">The Method to invoke when invoking this PluginFunction..</param>
    ''' <param name="plugin">The Plugin that is the Parent of this PluginFunction. This value can be NULL.</param>
    ''' <param name="params">The Parameters that was given by the attribute.</param>
    ''' <param name="funcCode">The function code for this PluginFunction.</param>
    Public Sub New(method As MethodInfo, plugin As Plugin, params As Object(), funcCode As String)
        Me.New(method, plugin)
        Me.Params = params
        Me.FunctionCode = funcCode
    End Sub

    ''' <summary>
    ''' Invokes the Method of the PluginFunction.
    ''' </summary>
    Public Sub Invoke()
        Method.Invoke(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' Invokes the Method of the PluginFunction.
    ''' </summary>
    Public Sub Invoke(ParamArray params As Object())
        Method.Invoke(Nothing, params)
    End Sub

    ''' <summary>
    ''' Invokes the Method of the PluginFunction and returns the return value.
    ''' </summary>
    Public Function InvokeGet(ParamArray params As Object()) As Object
        Return Method.Invoke(Nothing, params)
    End Function

End Class
