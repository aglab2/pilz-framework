﻿Namespace CameraN

    Public Enum LookDirection
        Top
        Bottom
        Left
        Right
        Front
        Back
    End Enum

End Namespace
