﻿Namespace CameraN

    Public Enum CameraMode
        FLY = 0
        ORBIT = 1
        LOOK_DIRECTION = 2
    End Enum

End Namespace
