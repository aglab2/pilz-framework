﻿Public Interface ISettingsManager
    Property ConfigFilePath As String
    Sub Save()
    Sub Load()
    Sub Reset()
End Interface