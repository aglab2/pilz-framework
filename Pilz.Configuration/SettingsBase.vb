﻿Imports Newtonsoft.Json

Public MustInherit Class SettingsBase

    Public Sub New()
        ResetValues()
    End Sub

    <JsonIgnore>
    Friend _settingsManager As ISettingsManager
    <JsonIgnore>
    Public ReadOnly Property SettingsManager As ISettingsManager
        Get
            Return _settingsManager
        End Get
    End Property

    Public MustOverride Sub ResetValues()

End Class
