﻿Public Class PaintingObjectLayering

    Public ReadOnly Property PaintingObject As PaintingObject

    ''' <summary>
    ''' Get the current object list from the painting object.
    ''' </summary>
    ''' <returns>Returns the current object list from the painting object.</returns>
    Public ReadOnly Property ObjectList As PaintingObjectList
        Get
            Return PaintingObject.Parent.PaintingObjects
        End Get
    End Property

    ''' <summary>
    ''' Create a new instance of object layer managing.
    ''' </summary>
    ''' <param name="obj"></param>
    Public Sub New(obj As PaintingObject)
        PaintingObject = obj
    End Sub

    ''' <summary>
    ''' Moves the object by the given number of indicies.
    ''' </summary>
    ''' <param name="count">The number how many objects it should be moved.</param>
    Public Sub MoveObject(count As Integer)
        Dim oldIndex As Integer = ObjectList.IndexOf(PaintingObject)
        Dim newIndex As Integer = oldIndex + count
        MoveObjectTo(newIndex)
    End Sub

    ''' <summary>
    ''' Moves the object to the new index.
    ''' </summary>
    ''' <param name="newIndex"></param>
    Public Sub MoveObjectTo(newIndex As Integer)
        Dim list As PaintingObjectList = ObjectList

        'Check & make index valid
        If newIndex >= ObjectList.Count Then
            newIndex = ObjectList.Count - 1
        ElseIf newIndex < 0 Then
            newIndex = 0
        End If

        'Remove object
        list.Remove(PaintingObject)

        'Insert object at new index
        list.Insert(newIndex, PaintingObject)

        'Order all objects again
        list.Layering.OrderAll()
    End Sub

    ''' <summary>
    ''' Moves the object to the front.
    ''' </summary>
    Public Sub BringToTop()
        MoveObjectTo(ObjectList.Count - 1)
    End Sub

    ''' <summary>
    ''' Moves the object to the back.
    ''' </summary>
    Public Sub SendToBack()
        MoveObjectTo(0)
    End Sub

    ''' <summary>
    ''' Moves the object fordward by one 
    ''' </summary>
    Public Sub OneToTop()
        MoveObject(+1)
    End Sub

    ''' <summary>
    ''' Moves the object backward by one
    ''' </summary>
    Public Sub OneToBack()
        MoveObject(-1)
    End Sub

End Class
