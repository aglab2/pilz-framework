﻿Imports System.Drawing
Imports System.Windows.Forms

Public Delegate Sub DelegateDrawPaintingObjectMethode(e As PaintingObjectPaintEventArgs)
Public Delegate Sub DelegateDrawPaintingControlGridMethode(e As PaintEventArgs, pc As PaintingControl, offset As PointF)
Public Delegate Sub DelegateDrawPaintingControlAreaSelectionMethode(e As PaintEventArgs, pc As PaintingControl, startMousePos As PointF, lastMousePos As PointF)
