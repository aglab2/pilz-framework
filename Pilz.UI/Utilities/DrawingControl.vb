﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms

Namespace Utils

    Public Module DrawingControl

        Private Const WM_SETREDRAW = 11

        <Extension>
        Public Sub SuspendDrawing(control As Control)
            SendMessage(control.Handle, WM_SETREDRAW, False, 0)
        End Sub

        <Extension>
        Public Sub ResumeDrawing(control As Control)
            ResumeDrawing(control, True)
        End Sub

        <Extension>
        Public Sub ResumeDrawing(control As Control, redraw As Boolean)
            SendMessage(control.Handle, WM_SETREDRAW, True, 0)
            If redraw Then control.Refresh()
        End Sub

    End Module

End Namespace
