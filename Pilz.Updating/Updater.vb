﻿Imports System.IO
Imports System.IO.Compression
Imports System.Net
Imports System.Reflection
Imports Newtonsoft.Json.Linq
Imports Pilz.Updating.Model
Imports Pilz.Updating.MyEventArgs

Public Class Updater

    Public Event UpdatesFound(sender As Object, e As UpdatesFoundEventArgs)
    Public Event NoUpdatesFound(sender As Object, e As EventArgs)
    Public Event UpdateInstallerStarted(Sender As Object, e As EventArgs)

    Public Property InstallAsAdmin As Boolean = True
    Public Property RestartHostApplication As Boolean = True
    Public Property AutomaticlyCloseHostApplication As Boolean = True
    Public Property UpdateDirURL As String
    Public Property UpdateInfoURL As String

    Public Sub New(updateUrl As String)
        UpdateInfoURL = updateUrl
        UpdateDirURL = updateUrl.Remove(updateUrl.LastIndexOf("/"c))
    End Sub

    Public Async Function Update() As Task
        Dim wc As New WebClient
        Dim updateInstallerZipPath As String = Path.Combine(MyTempPath, "Pilz.Updating.UpdateInstaller.zip")
        Dim updateInstallerPath As String = updateInstallerZipPath & "_unpacked"
        Dim updateInstallerExePath As String = Path.Combine(updateInstallerPath, "Pilz.Updating.UpdateInstaller.exe")

        'Clean AppData
        For Each dir As String In Directory.GetDirectories(MyTempPath, "*", SearchOption.TopDirectoryOnly)
            Directory.Delete(dir, True)
        Next

        'Download UpdateInfos
        Dim strUpdateInfos As String = Await wc.DownloadStringTaskAsync(UpdateInfoURL)
        Dim updateInfos As UpdateInfo() = JArray.Parse(strUpdateInfos).ToObject(Of UpdateInfo())

        If updateInfos.Any Then
            'Order update infos from newest to oldest
            updateInfos = updateInfos.OrderByDescending(
                    Function(n)
                        Return n.Version.ToString &
                        (Byte.MaxValue - n.State).ToString("X2") &
                        n.Build.ToString("X4")
                    End Function).ToArray

            'Interagate with user
            Dim e As New UpdatesFoundEventArgs(updateInfos)
            RaiseEvent UpdatesFound(Me, e)

            'Install updates
            If Not e.Cancel Then
                Dim updateInfo As UpdateInfo = updateInfos(0)
                Dim updatePackageZipFilename As String = $"{updateInfo.Version.ToString} {CByte(updateInfo.State)} {updateInfo.Build}.zip"
                Dim updatePackageZipPath As String = Path.Combine(MyTempPath, updatePackageZipFilename)
                Dim updatePackagePath As String = updatePackageZipPath & "_unpacked"

                'Download UpdateInstaller
                Await wc.DownloadFileTaskAsync(UpdateDirURL & "/Pilz.Updating.UpdateInstaller.zip", updateInstallerZipPath)
                ZipFile.ExtractToDirectory(updateInstallerZipPath, updateInstallerPath)

                'Download Update Package
                Await wc.DownloadFileTaskAsync(UpdateDirURL & "/" & updatePackageZipFilename, updatePackageZipPath)
                ZipFile.ExtractToDirectory(updatePackageZipFilename, updatePackagePath)

                'Build process arguments
                Dim applicationExePath As String = Assembly.GetExecutingAssembly.Location
                Dim procArgs As String = $"restartHostApp#{RestartHostApplication} autoCloseHostApp#{AutomaticlyCloseHostApplication} ""updatePackage#{updatePackagePath}"" ""applicationPath#{applicationExePath}"""

                'Finally start update installer to install updates
                Process.Start(updateInstallerExePath, procArgs)

                'Interagate with user
                RaiseEvent UpdateInstallerStarted(Me, New EventArgs)
            End If
        Else
            RaiseEvent NoUpdatesFound(Me, New EventArgs)
        End If

        wc.Dispose()
    End Function

End Class
