﻿Namespace Model

    Public Class UpdateInfo

        Public Property Name As String
        Public Property Version As Version
        Public Property State As BuildState
        Public Property Build As UInteger
        Public Property Changelog As String

    End Class

End Namespace
