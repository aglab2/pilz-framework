﻿Namespace Model

    Public Enum BuildState
        Release
        ReleaseCandidate
        Beta
        Alpha
    End Enum

End Namespace
