﻿Imports Pilz.Updating.Scripts

Namespace Model

    Public Class UpdatePackage

        Public ReadOnly Property Fileupdates As New List(Of String)
        Public ReadOnly Property Scripts As New List(Of Script)

    End Class

End Namespace
