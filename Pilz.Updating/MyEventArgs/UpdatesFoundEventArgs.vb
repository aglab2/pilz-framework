﻿Imports Pilz.Updating.Model

Namespace MyEventArgs

    Public Class UpdatesFoundEventArgs
        Inherits EventArgs

        Public Property Cancel As Boolean = False
        Public Property Updates As UpdateInfo()

        Public Sub New(updates As UpdateInfo())
            Me.Updates = updates
        End Sub

    End Class

End Namespace
