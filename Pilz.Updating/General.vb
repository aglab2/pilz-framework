﻿Imports System.IO

Public Module General

    Public ReadOnly Property MyTempPath As String
        Get
            Static p As String = String.Empty

            If String.IsNullOrEmpty(p) Then
                p = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Pilz.Updating")

                If Not Directory.Exists(p) Then
                    Directory.CreateDirectory(p)
                End If
            End If

            Return p
        End Get
    End Property

End Module
