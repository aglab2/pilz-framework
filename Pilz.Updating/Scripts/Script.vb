﻿Imports Pilz.Updating.Model

Namespace Scripts

    Public Class Script

        Public Property Priority As ScriptPriority = ScriptPriority.Before
        Public Property Language As CodeLanguage = CodeLanguage.CSharp
        Public Property Code As String

    End Class

End Namespace
