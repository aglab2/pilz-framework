﻿Imports System.Numerics

Public Class Mesh

    Public ReadOnly Property Vertices As New List(Of Vertex)
    Public ReadOnly Property Normals As New List(Of Normal)
    Public ReadOnly Property UVs As New List(Of UV)
    Public ReadOnly Property VertexColors As New List(Of VertexColor)
    Public ReadOnly Property Faces As New List(Of Face)

    Public Sub CenterModel()
        CenterModel({Me})
    End Sub

    Public Shared Sub CenterModel(meshes As IEnumerable(Of Mesh))
        Dim avgX As Integer = 0
        Dim avgY As Integer = 0
        Dim avgZ As Integer = 0
        Dim vertsCount As Long = 0

        For Each m As Mesh In meshes
            For Each v As Vertex In m.Vertices
                avgX += v.X
                avgY += v.Y
                avgZ += v.Z
            Next
            vertsCount += m.Vertices.Count
        Next

        Dim avg As New Vector3(avgX, avgY, avgZ)
        avg /= New Vector3(vertsCount)

        For Each m As Mesh In meshes
            For Each v As Vertex In m.Vertices
                v.X -= avg.X
                v.Y -= avg.Y
                v.Z -= avg.Z
            Next
        Next
    End Sub

End Class
